import React, { Component } from "react";
const logo = "../static/svg/logo.svg";
const logo_i = "../static/svg/logo_i.svg";
import Link from "../components/Link";
export default class Nav extends Component {
  render() {
    const { i } = this.props;
    return (
      <nav>
        <div className="container">
          <div className="logo">
            <Link href="/">
              <a>
                <img src={i ? logo_i : logo} alt="" />
              </a>
            </Link>
          </div>
          <ul>
            <li>
              <Link href="/" activeClassName="active">
                <a>Home</a>
              </Link>
            </li>
            <li>
              <Link href="/projects" activeClassName="active">
                <a>Projects</a>
              </Link>
            </li>
            <li>
              <Link href="/team" activeClassName="active">
                <a>Team</a>
              </Link>
            </li>
            <li>
              <Link href="/about" activeClassName="active">
                <a>About</a>
              </Link>
            </li>
            <li>
              <Link href="/contact" activeClassName="active">
                <a>Contact</a>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
