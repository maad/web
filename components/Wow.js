import React, { Component } from "react";
import ReactWOW from "react-wow";
export default class Wow extends Component {
  render() {
    return (
      <ReactWOW animation={this.props.animation || "fadeIn"} {...this.props}>
        {this.props.children}
      </ReactWOW>
    );
  }
}
