import React, { Component } from "react";
import Head from "next/head";
export default class PageMeta extends Component {
  render() {
    const meta = {
      "/": {
        title: "Maad Audio | Audio & Music app development",
        description:
          "A passionate team of developers, musicians and artists operating in the music software industry. We specialize in the development and design of audio and music applications for Mac, Windows, iOS, Android and the Web."
      },
      "/projects": {
        title: "Projects | Maad Audio ",
        description:
          "Our projects and collaborations. See examples of our work and projects we took part on."
      },
      "/team": {
        title: "Our team | Maad Audio ",
        description:
          "Meet the Maad Audio development team. Our agile development approach makes us deliver quality software at competitive fees."
      },
      "/skills": {
        title: "Our skills | Maad Audio ",
        description:
          "The languages, frameworks and technologies we mastered over the years. We also love to take on new problems, and face challanges with passion and determination."
      },
      "/about": {
        title: "About | Maad Audio ",
        description:
          "Our work focuses on the conception, prototyping, development and distribution of audio and music software applications."
      },
      "/contact": {
        title: "Contact us | Maad Audio ",
        description:
          "Collaborate with us, enquire about a project, or talk with the team."
      }
    };
    const { path } = this.props;
    const m = meta[path] || meta["/"];
    return (
      <Head>
        <title>{m.title}</title>
        <meta name="description" content={m.description} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/static/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/static/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/static/favicon-16x16.png"
        />
        <link rel="manifest" href="/static/site.webmanifest" />
        <link
          rel="mask-icon"
          href="/static/safari-pinned-tab.svg"
          color="#fca924"
        />
        <link rel="shortcut icon" href="/static/favicon.ico" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="msapplication-config" content="/static/browserconfig.xml" />
        <meta name="og:title" content={m.title} />
        <meta
          name="og:image"
          content="https://maad.netlify.com/static/bg_text.jpg"
        />
        <meta name="og:description" content={m.description} />
        <meta name="theme-color" content="#ffffff" />
      </Head>
    );
  }
}
