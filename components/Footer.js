import React, { Component } from "react";
import Link from "./Link";
const logo = "/static/svg/logo.svg";
const logo_i = "/static/svg/logo_i.svg";
export default class Footer extends Component {
  render() {
    const { i } = this.props;
    return (
      <footer>
        <div className="container">
          <h1 className="logo">
            <Link href="/">
              <a>
                <img src={i ? logo_i : logo} alt="" />
                maad <span>/</span> audio
              </a>
            </Link>
          </h1>
        </div>
      </footer>
    );
  }
}
