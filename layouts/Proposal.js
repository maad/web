import React, { Component } from "react";
import Nav from "../components/Nav";
import Footer from "../components/Footer";
import Link from "../components/Link";
const logo_c = "/static/svg/logo_c.svg";
export default class Layout extends Component {
  render() {
    return (
      <div className="bgd">
        <div className="page i proposal">
          {/* <Nav i={true} /> */}
          <nav>
            <div className="container">
              <div className="logo">
                <Link href="/">
                  <a>
                    <img src={logo_c} alt="" /> maad <span>/</span> audio
                  </a>
                </Link>
              </div>
              <ul>
                <li>
                  <span>Project proposal</span>
                </li>
              </ul>
            </div>
          </nav>
          <div className="view">
            <div>{this.props.children}</div>
          </div>

          <Footer />
        </div>
      </div>
    );
  }
}
