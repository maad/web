import React, { Component } from "react";
import Nav from "../components/Nav";
import Link from "../components/Link";
import { PageTransition } from "next-page-transitions";
import Head from "next/head";
import Wow from "../components/Wow";
export default class Layout extends Component {
  render() {
    return (
      <div className="home page bgd">
        <div className="bg" />
        <Nav />

        <div className="view">
          <div>{this.props.children}</div>
        </div>

        <div className="msg">
          <div className="container">
            <div className="l">
              <Wow delay=".8s">
                <p>
                  We develop audio and music software solutions. Contact us to
                  discuss a project or collaboration.
                </p>
              </Wow>
            </div>
            <div className="r">
              <Wow delay="1.1s">
                <p>
                  <Link href="/contact">
                    <a className="btn">Let's talk</a>
                  </Link>
                </p>
              </Wow>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
