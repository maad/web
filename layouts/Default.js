import React, { Component } from "react";
import Nav from "../components/Nav";
import Footer from "../components/Footer";
import { PageTransition } from "next-page-transitions";
export default class Layout extends Component {
  render() {
    return (
      <div className="bgd">
        <div className="bg" />
        <div className="page">
          <Nav />
          {/* <PageTransition
            timeout={300}
            classNames="page-transition"
            loadingComponent={<div />}
            loadingDelay={100}
            loadingTimeout={{
              enter: 100,
              exit: 100
            }}
            monkeyPatchScrolling={true}
            loadingClassNames="loading-indicator"
          > */}
          <div className="view">
            <div>{this.props.children}</div>
          </div>
          {/* </PageTransition> */}
          <Footer />
        </div>
      </div>
    );
  }
}
