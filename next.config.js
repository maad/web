const withPlugins = require("next-compose-plugins");
const css = require("@zeit/next-css");
const images = require("next-images");
/**
 * Config file for next.js
 */

module.exports = withPlugins([css, images]);
