/**
 * Next.js uses the App component to initialize all the pages.
 * You can override it and control the page initialization.
 */
import App, { Container } from "next/app";

import Router from "next/router";
import "animate.css";
import React from "react";
import "../css/app.css";
import PageMeta from "../components/PageMeta";
// import { library } from "@fortawesome/fontawesome-svg-core";
// import { fab } from "@fortawesome/free-brands-svg-icons";
// import { far } from "@fortawesome/free-regular-svg-icons";
// import { fas } from "@fortawesome/free-solid-svg-icons";

// library.add(fab, far, fas);

// Router.onRouteChangeStart = url => {
//   NProgress.start();
// };
// Router.onRouteChangeComplete = () => NProgress.done();
// Router.onRouteChangeError = () => NProgress.done();

export default class Maad extends App {
  static async getInitialProps({ Component, router, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    return { pageProps };
  }

  render() {
    const { Component, router, pageProps } = this.props;

    return (
      <Container>
        <PageMeta path={router.route} />
        <Component {...pageProps} />
      </Container>
    );
  }
}
