import React, { Component } from "react";
const logo_c = "../static/svg/logo_c.svg";
import Layout from "../layouts/Home.js";
import Wow from "../components/Wow";
export default class index extends Component {
  render() {
    return (
      <Layout>
        <section>
          <div className="container">
            <Wow delay=".3s">
              <h1 className="logo">
                <img src={logo_c} alt="" />
                maad <span>/</span> audio
              </h1>
            </Wow>
            <Wow delay=".5s" animation="fadeInUp">
              <h2>Software development &amp; design</h2>
            </Wow>
          </div>
        </section>
      </Layout>
    );
  }
}
