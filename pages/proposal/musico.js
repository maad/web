import React, { Component } from "react";
import Layout from "../../layouts/Proposal.js";
export default class about extends Component {
  render() {
    const p = {};
    return (
      <Layout>
        <section>
          <div className="container">
            <h1>Musico</h1>
            <h2>Cloud solution / Web app / Cross media plugin</h2>

            <div className="project">
              <h3>Cloud solution</h3>
              <h4>
                Client + server architecture
                <span className="estimates">
                  <span>Estimate :</span> 6 months / €8750
                </span>
              </h4>
              <div>
                <h5>Components</h5>
                <ul>
                  <li>
                    Audio engine: compiled libPd library or PureData app (server
                    side)
                  </li>
                  <li>
                    Possible bridge between audio engine and network (server
                    side)
                  </li>
                  <li>
                    Web RTC implementation for real-time client-server
                    interaction (client side)
                  </li>
                  <li>
                    Client Web App respecting Musico's branding and UX
                    guidelines (client side)
                  </li>
                </ul>
              </div>
              <div>
                <h5>Pros</h5>
                <ul>
                  <li>
                    No problems related to 'hiding' or masking the patch since
                    it would be inaccessible (on the server).
                  </li>
                </ul>
              </div>
              <div>
                <h5>Cons</h5>
                <ul>
                  <li>
                    Server side costs directly proportionalte to the number of
                    connections (more Pd multi-instances -> more resources)
                  </li>
                  <li>
                    Scalability of the multi-instanced Pd still to verify and
                    analyze.
                  </li>
                  <li>
                    Longer testing and pre-production periods due to the
                    scaling/dimensions of the server architecture.
                  </li>
                  <li>Latency issues</li>
                  <li>Resources to allocate for each Pd istance</li>
                </ul>
              </div>

              <div className="project">
                <h3>Web app</h3>
                <h4>
                  Client only solution
                  <span className="estimates">
                    <span>Estimate :</span> 3 months / €3750
                  </span>
                </h4>
                <div>
                  <h5>Components</h5>
                  <ul>
                    <li>webPd for audio engine (client side)</li>
                    <li>
                      Client Web App respecting Musico's branding and UX
                      guidelines (client side)
                    </li>
                  </ul>
                </div>
                <div>
                  <h5>Pros</h5>
                  <ul>
                    <li>No server-side costs</li>

                    <li>No Pd multi-instance problems</li>
                    <li>Easier and cheaper scalability</li>
                    <li>smaller development costs</li>
                    <li>shorter development time</li>
                  </ul>
                </div>
                <div>
                  <h5>Cons</h5>
                  <ul>
                    <li>Hiding or 'masking' the patch.</li>
                    <li>
                      webPd is now out as a beta release -> probably will
                      require some bugfixing
                    </li>
                    <li>
                      Not all objects used in the patch have been implemented in
                      webPd.
                    </li>
                  </ul>
                </div>
              </div>

              <div className="project">
                <h3>Wwise plugin</h3>
                <h4>
                  Plugin solution
                  <span className="estimates">
                    <span>Estimate :</span> 5 months / €6250
                  </span>
                </h4>
                <div>
                  <h5>Components</h5>
                  <ul>
                    <li>Generic GUI</li>
                    <li>Sound engine module</li>
                    <li>Test app</li>
                    <li>Distribution</li>
                  </ul>
                </div>
                <div>
                  <h5>Pros</h5>
                  <ul>
                    <li>Compatibile with Unity and Unreal</li>
                  </ul>
                </div>
                <div>
                  <h5>Cons</h5>
                  <ul>
                    <li>
                      Very rigid requirements (
                      <a href="https://www.audiokinetic.com/library/edge/?source=SDK&id=plugin__tests.html">
                        https://www.audiokinetic.com/library/edge/?source=SDK&id=plugin__tests.html
                      </a>
                      )
                    </li>
                    <li>
                      Will probably have to modify the libPd framework (for
                      meeting requirements)
                    </li>
                    <li>
                      These uncertainties may slow development time or give rise
                      to less-expected problems during the development time (see
                      Unity/Unreal plugin for better solution)
                    </li>
                  </ul>
                </div>
              </div>

              <div className="project">
                <h3>Unity/Unreal plugin</h3>
                <h4>
                  Plugin solution
                  <span className="estimates">
                    <span>Estimate :</span> 3 months / €3750
                  </span>
                </h4>

                <div>
                  <h5>Pros</h5>
                  <ul>
                    <li>Accessible requirements</li>
                    <li>Documentation and support are very well done</li>
                  </ul>
                </div>
                <div>
                  <h5>Cons</h5>
                  <ul>
                    <li>
                      The only con is that in the case of Unreal, there may be
                      delays due to the use of the Unreal 4 IDE.
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="notes">
              <h3>Notes</h3>
              <p>
                Maintenance costs and timeframes should be estimated when
                development is complete.
                <br />
                All price figures are gross (include tax).
              </p>
            </div>
          </div>
        </section>
      </Layout>
    );
  }
}
