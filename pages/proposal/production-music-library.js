import React, { Component } from "react";
import Layout from "../../layouts/Proposal.js";
export default class about extends Component {
  render() {
    const p = {};
    return (
      <Layout>
        <section>
          <div className="container">
            <h1>Production Music Library</h1>
            <h2>Web app con funzioni Web Audio + Db/Server setup</h2>

            <div className="project">
              <h3>Full version</h3>
              <h4>
                Sviluppo di tutte le funzioni client e custom backend
                <span className="estimates">
                  <span>Estimate :</span> 3 mesi / €5375
                </span>
              </h4>
              <div>
                <h5>Componenti</h5>
                <br />
                <h4>Database/Backend</h4>
                <ul>
                  <li>
                    Setup istanza database (PostgreSQL) su piattaforma cloud
                    (GCP, AWS, DigitalOcean, Heroku) o altro hosting preferito
                    dal cliente
                  </li>
                  <li>Autenticazione</li>
                  <li>
                    4 tipi di utenti/permessi (amministratore, sviluppatore,
                    fornitore, utente)
                  </li>
                  <li>
                    API per la comunicazione con il database (NodeJS/GraphQL)
                  </li>
                  <li>
                    Creazione di bucket di memoria su cloud (Cloud Storage) o
                    Server/Filesystem preferito dal cliente per l'archiviazione
                    dei file audio della piattaforma.
                  </li>
                </ul>
                <br />
                <h4>Web Application</h4>
                <ul>
                  <li>
                    Web Application per browser moderni scritta ad hoc
                    <ul>
                      <li>
                        Sviluppo, design e deployment di una Web Application
                        (ReactJS) su App Engine (GCP) o altro PaaS/hosting
                        preferito dal cliente
                      </li>
                      <li>Ricerca ed esplorazione dei contenuti e metadati</li>
                      <li>Profili utente</li>
                      <li>
                        Interfaccia per il live mixing/editing/download di mix
                        alternativi (Web Audio API, ReactJS)
                      </li>
                    </ul>
                  </li>
                  <li>
                    Pennello di amministrazione per il caricamento/gestione dei
                    file e metadati (per amministratori e livelli privilegiati
                    di utenti).
                  </li>
                  <li>Design grafico moderno ed intuitivo</li>
                </ul>
              </div>
            </div>

            <div className="project">
              <h3>Maintenance/Funzioni aggiuntive</h3>
              <h4>
                Mantenimento della piattaforma, estensione con nuove feature,
                risoluzioni di problemi
                <span className="estimates">
                  <span>Estimate :</span> On-going / hourly or fixed rate
                </span>
              </h4>
              <div>
                <h5>Attività</h5>
                <ul>
                  <li>
                    Sviluppo nuove funzionalità sull'intero stack ed estensione
                    della piattaforma
                  </li>
                  <li>
                    Risoluzione di problemi emergenti (non inclusi o prevedibili
                    nella consegna iniziale)
                  </li>
                  <li>
                    Aggiornamento continuo di librerie e software sull'intero
                    stack per rimanere in linea con le ultime best-practices
                    fornite dai browser e device.
                  </li>
                  <li>
                    DB administration, estensione dello schema per nuove
                    funzionalità
                  </li>
                </ul>
                <br />
                <h4>Piano orario</h4>
                <ul>
                  <li>
                    Interventi di maintenance occasionali sull'intero stack
                    possono essere richiesti ad un costo orario di €24/h,
                  </li>
                </ul>
                <h4>Piano mensile</h4>
                <ul>
                  <li>
                    Una copertura di maintenance continuativa può essere
                    garantita a un rate mensile di €500, entro un massimo di 10
                    ore settimanali.
                  </li>
                </ul>
                <h4>Per richiesta</h4>
                <ul>
                  <li>
                    Nuove feature o interventi di manutenzione specifici possono
                    essere concordati a prezzi e costi stabiliti con il cliente
                  </li>
                </ul>
              </div>
            </div>

            <div className="notes">
              <h3>Note</h3>
              <p className="s">
                Costi di Cloud/Hosting non inclusi
                <br />
                Compliance con il GDPR rimane di responsabilità del committente.
                Si implementeranno gli essenziali tecnici per essere in regola,
                ma questo e altri issues legali (nonchè stesura dei termini e
                condizioni del servizio) non inclusi.
                <br />
                Tutti i prezzi sono lordi (tasse incluse).
              </p>
            </div>
          </div>
        </section>
      </Layout>
    );
  }
}
