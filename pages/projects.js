import React, { Component } from "react";
import Layout from "../layouts/Default.js";
import Link from "../components/Link.js";
import Wow from "../components/Wow";
export default class about extends Component {
  render() {
    return (
      <Layout>
        <section className="c">
          <div className="container">
            <Wow delay=".3s">
              <h1>Our projects</h1>
              <h2>Page coming soon...</h2>
              <p>
                Our full portfolio with demos and work examples will be online
                soon.
                <br />
                If you're intersted in knowing more about our work, please{" "}
                <Link href="/contact">
                  <a>contact us</a>
                </Link>
                .
              </p>
            </Wow>
          </div>
        </section>
      </Layout>
    );
  }
}
