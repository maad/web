import React, { Component } from "react";
import Layout from "../layouts/Default.js";
import Link from "../components/Link.js";
import Wow from "../components/Wow";
export default class about extends Component {
  render() {
    return (
      <Layout>
        <section className="portfolio c ">
          <div className="container">
            <Wow delay=".3s">
              <h1>Our projects</h1>
              <div className="category">
                <h3>App development</h3>
                <h4>iOS, Android &amp; Desktop apps</h4>
              </div>
              <div className="category">
                <h3>Design projects</h3>
                <h4>GUI design / UX design</h4>
              </div>
              <div className="category">
                <h3>Collaborations</h3>
                <h4>Awesome people we worked with</h4>
                <img
                  src="https://www.plantsplay.com/wp-content/themes/plantsplay_front/img/primary-color-positive-tagline-black-background.svg"
                  alt=""
                  width="200px"
                />
                <img
                  src="https://www.elliottgarage.com/img/logoWhite.png"
                  alt=""
                  width="300px"
                />
              </div>
              <p>
                All the above projects and collaborations have involved at least
                one member/skillset of the MAAD team.
                <br />
                If you're intersted in knowing more about our work, please{" "}
                <Link href="/contact">
                  <a>contact us</a>
                </Link>
                .
              </p>
            </Wow>
          </div>
        </section>
      </Layout>
    );
  }
}
