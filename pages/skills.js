import React, { Component } from "react";
import Layout from "../layouts/Default.js";
import Link from "../components/Link";
import Wow from "../components/Wow.js";
export default class about extends Component {
  render() {
    const skills = {
      coding: [
        { l: "C / C++", v: 9.5 },
        { l: "Objective C", v: 9.5 },
        { l: "C#", v: 8 },
        { l: "Java", v: 7 },
        { l: "Javascript", v: 9.5 },
        { l: "HTML5 / CSS3", v: 9.5 },
        { l: "Php", v: 7 },
        { l: "Python", v: 6 },
        { l: "Ruby", v: 6 }
      ],
      frameworks: [
        { l: "JUCE", v: 9.5 },
        { l: "ReactJS", v: 9.5 },
        { l: "Node Js", v: 8 },
        { l: "Firebase", v: 8 },
        { l: "Web Audio", v: 8 },
        { l: "Web MIDI", v: 7 },
        { l: "Webpack", v: 7 },
        { l: "AngularJs", v: 8 },
        { l: "Wordpress", v: 8 }
      ],
      technologies: [
        { l: "Git/SVN", v: 9.5 },
        { l: "VST", v: 9.5 },
        { l: "AU / AU3", v: 9.5 },
        { l: "PureData / libPd", v: 9.5 },
        { l: "Tensorflow", v: 6 },
        { l: "AWS", v: 7.5 },
        { l: "Google Cloud", v: 7 }
      ],
      graphics: [
        { l: "Photoshop", v: 9.5 },
        { l: "Illustrator", v: 9.5 },
        { l: "After Effects", v: 8 },
        { l: "Sketch", v: 7 },
        { l: "FinalCut", v: 7 },
        { l: "SVG", v: 8 }
      ],
      services: [
        { l: "Tech consulting", v: 9.5 },
        { l: "Prototyping", v: 9.5 },
        { l: "SEO / ASO", v: 7.5 },
        { l: "Digital marketing", v: 7.5 },
        { l: "Analytics", v: 8 }
      ]
    };
    return (
      <Layout>
        <section>
          <div className="container">
            <Wow>
              <h1>Our Skills</h1>
              <h2>What we're good at </h2>
            </Wow>
          </div>
        </section>
        <section>
          <div className="container">
            <Wow>
              <div className="skills">
                {Object.keys(skills).map((s, i) => {
                  return (
                    <div className="skillset">
                      <h3>/ {s}</h3>
                      <div>
                        {skills[s].map(sk => (
                          <SkillBar sk={sk} />
                        ))}
                      </div>
                    </div>
                  );
                })}
              </div>
            </Wow>
          </div>
        </section>
        <section className="no-p">
          <Wow>
            <div className="container">
              <p className="s">
                These are the languages and technologies we use in our daily
                work, but our most valuable skill is by far the ability to adapt
                and take on new problems, facing challanges with passion,
                dedication and determination. This attitude allows us to deliver
                quality work as a team, and consider few things as impossible!
              </p>
              <p className="s">
                Want to discuss a project or collaboration, or need the team's
                help in any way?{" "}
                <Link href="/contact">
                  <a>Contact us</a>
                </Link>
                .
              </p>
            </div>
          </Wow>
        </section>
      </Layout>
    );
  }
}

class SkillBar extends Component {
  render() {
    const { sk } = this.props;
    return (
      <div className="skillbar">
        <div className="skill">{sk.l}</div>
        <div className="experience">
          <div
            style={{
              width: sk.v * 10 + "%"
            }}
          />
        </div>
      </div>
    );
  }
}
