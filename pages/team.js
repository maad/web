import React, { Component } from "react";
import Layout from "../layouts/Default.js";
import Wow from "../components/Wow";
import Link from "../components/Link.js";
export default class about extends Component {
  render() {
    return (
      <Layout>
        <section className="c">
          <div className="container">
            <Wow animation="fadeIn" delay=".3s">
              <h1>Our team</h1>
              <h2>Developers / Designers / Musicians</h2>
            </Wow>
          </div>
        </section>
        <section>
          <div className="container">
            <Wow animation="fadeIn" delay=".5s">
              <div className="team-member">
                <img src="/static/marco.jpg" alt="" />
                <h3>
                  <span>M</span>arco B.
                </h3>
                <h4>Dev</h4>
                <p>
                  Marco is a specialist in contemporary audio and music
                  technologies, and teaches Sound Engineering at the University
                  of Rome.
                </p>
              </div>
            </Wow>

            <Wow animation="fadeIn" delay=".7s">
              <div className="team-member">
                <img src="/static/andreaa.jpg" alt="" />
                <h3>
                  <span>A</span>ndrea A.
                </h3>
                <h4>Dev</h4>
                <p>
                  Developing his own synths and apps from an early age and under
                  his own brand, Andrea is an iOS wizard and a sound design
                  innovator.
                </p>
              </div>
            </Wow>

            <Wow animation="fadeIn" delay=".9s">
              <div className="team-member">
                <img src="/static/andreas.jpg" alt="" />
                <h3>
                  <span>A</span>ndrea S.
                </h3>
                <h4>Dev</h4>
                <p>
                  Andrea's horizontal technical expertise in a wide variety of
                  areas of software make him the ideal architect for almost any
                  conceivable project.
                </p>
              </div>
            </Wow>

            <Wow animation="fadeIn" delay="1.1s">
              <div className="team-member">
                <img src="/static/dario.png" alt="" />
                <h3>
                  <span>D</span>ario K.
                </h3>
                <h4>Ui / Ux</h4>
                <p>
                  Dario's talent as a designer, producer, and experienced music
                  tech user ensures our apps stay fresh and relevant to today's
                  market.
                </p>
              </div>
            </Wow>
          </div>
        </section>
        <section className="c">
          <Wow animation="fadeIn" delay="1.5s">
            <p style={{ fontSize: "1rem" }}>
              <Link href="/skills">
                <a>Our skills ></a>
              </Link>
            </p>
          </Wow>
        </section>
      </Layout>
    );
  }
}
