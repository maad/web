import React, { Component } from "react";
import Layout from "../layouts/Default.js";
import Wow from "../components/Wow.js";
import Link from "../components/Link.js";
export default class about extends Component {
  render() {
    return (
      <Layout>
        <section className="c">
          <div className="container">
            <Wow delay=".3s">
              <div className="l d-ib" style={{ maxWidth: "700px" }}>
                <h1>We make what we love</h1>
                <h2>Cross-platform apps / UX design</h2>
                <p>
                  We're a passionate team of musicians, developers and artists
                  operating in the music software industry for over 10 years. We
                  specialize in the development and design of cross platform
                  audio and music applications (Mac, Windows, iOS, Android and
                  Web).
                </p>
                <p>
                  Our work focuses on the conception, prototyping, development
                  and distribution of apps and software in which sound or music
                  play a role. Our goal is to enable users and musicians to
                  enjoy software products playfully and intuitively, and merge
                  the latest trends in technology with the requirements of
                  stability and user-friendliness of the modern musician.
                </p>
                <p>
                  We welcome partnerships with industry companies, academic
                  institutions, startups and students. We're interested in
                  helping innovators create their products, offering the skills
                  and talents we gathered in years of research and expertise in
                  music technology to any project that stimulates our interest
                  in sound and its infinite applications.
                </p>
                <p>
                  Learn more about our{" "}
                  <Link href="/skills">
                    <a>skills and competences</a>
                  </Link>
                  , or meet{" "}
                  <Link href="/team">
                    <a>our team</a>
                  </Link>
                  .
                </p>
              </div>
            </Wow>
          </div>
        </section>
      </Layout>
    );
  }
}
