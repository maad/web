import React, { Component } from "react";
import Layout from "../layouts/Default";
import Wow from "../components/Wow";

export default class contacts extends Component {
  render() {
    return (
      <Layout>
        <section className="c">
          <div className="container">
            <Wow delay=".3s">
              <h1>Write to us</h1>
              <h2>Discuss a project / Discover our work.</h2>
              <p>
                <a href="mailto:maadaudioteam@gmail.com">
                  maadaudioteam@gmail.com
                </a>
              </p>
            </Wow>
          </div>
        </section>
      </Layout>
    );
  }
}
